# Recipe App API proxy

NGINX proxy for our recipe project

## Usage

### Environment variables

* `LISTEN_PORT` - Port to listen to (default: `8000`)
* `APP_HOST` - Hostname of the app (default: `app`)
* `APP_PORT` - Port of the app to forward request to (default: `9000`)
